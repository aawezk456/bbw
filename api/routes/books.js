const express = require('express');
const router = express.Router();
const multer = require('multer');
const Book = require('../models/book');
const fs = require('fs');
const mongoose = require('mongoose');
const { count } = require('console');

const async = require('async');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {

        var dir = "./uploaded_books";
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        cb(null, './uploaded_books/');


    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString() + ' ' + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage, limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

router.post('/saveBook', upload.array('book_img', 3), (req, res, next) => {
    const book = new Book({
        _id: new mongoose.Types.ObjectId(),
        book_name: req.body.book_name,
        author_name: req.body.author_name,
        Isbn_no: req.body.Isbn_no,
        book_img: [
            req.files[0].path,
            req.files[1].path,
            req.files[2].path
        ],
        publisher: req.body.publisher,
        condition: req.body.condition,
        print_type: req.body.print_type,
        mrp: req.body.mrp,
        selling_price: req.body.selling_price,
        saved_price: req.body.saved_price,
        sale_price: req.body.sale_price,
        description: req.body.description,
        publication_year: req.body.publication_year,
        quantity: req.body.quantity,
        no_Of_pages: req.body.no_Of_pages,
        language: req.body.language,
        dimensions: req.body.dimensions,
        weight: req.body.weight,
        categories: req.body.categories
    });
    book.save().then(result => {
        console.log(result);
        res.status(201).json({
            message: 'Created Product Successfully',
            createdProduct: {
                _id: result._id,
                book_name: result.book_name,
                author_name: result.author_name,
                Isbn_no: result.Isbn_no,
                book_img:
                    result.book_img
                ,
                publisher: result.publisher,
                condition: result.condition,
                print_type: result.print_type,
                mrp: result.mrp,
                selling_price: result.selling_price,
                saved_price: result.saved_price,
                sale_price: result.sale_price,
                description: result.description,
                publication_year: result.publication_year,
                quantity: result.quantity,
                no_Of_pages: result.no_Of_pages,
                language: result.language,
                dimensions: result.dimensions,
                weight: result.weight,
                categories: result.categories,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/products/' + result._id
                }
            }
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});



router.delete('/:bookId', (req, res, next) => {
    const id = req.params.bookId;
    Book.deleteOne({ _id: id }).exec()
        .then(result => {
            res.status(200).json({
                message: "Book deleted Successfully",
                result
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
});

router.get('/:categories', (req, res, next) => {
    const perPage = 10;
    const page = req.query.page;
    async.parallel([
        function(callback){
            Book.count({categories : req.params.categories },(err,count)=>{
                var totalBooks = count;
                callback(err,totalBooks);
            });
        },
        function(callback){
            Book.find({categories : req.params.categories})
            .skip(perPage * page)
            .limit(perPage)
            .exec((err,books)=>{
                if(err) return next(err);
                callback(err,books);
            });
        },
        function(callback){
            Book.findOne({categories : req.params.categories},(err,categories)=>{
               callback(err,categories)
            });
        }
    ],function(err,results){
        var totalBooks = results[0];
        var books = results[1];
        
        res.json({
            success : true,
            books : books,
            totalBooks : totalBooks,
            pages : Math.ceil(totalBooks/perPage - 1)
        });
      
    });
   
}
);

router.get('/', (req, res, next) => {
    const perPage = 10;
    const page = req.query.page;
    async.parallel([
        function(callback){
            Book.count({},(err,count)=>{
                var totalBooks = count;
                callback(err,totalBooks);
            });

        },
        function(callback){
            Book.find({})
            .skip(perPage * page)
            .limit(perPage)
            .exec((err,books)=>{
                if(err) return next(err);
                callback(err,books);
            });
        }
    ],function(err,results){
        var totalBooks = results[0];
        var books = results[1];

        res.json({
            success : true,
            books : books,
            totalBooks : totalBooks,
            pages : Math.ceil(totalBooks/perPage - 1)
        });
    });
});


router.get('/getid/:bookId', (req, res, next) => {
    const id = req.params.bookId;
    Book.find({ _id: id })
        .exec()
        .then(doc => {
            console.log(doc);
            if (doc) {
                res.status(200).json(doc);
            } else {
                res.status(404).json({
                    message: "No valid enrty found"
                });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
}
);


module.exports = router;