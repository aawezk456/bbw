const mongoose = require('mongoose');

const bookSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    
    book_name: {
        type: String,
        required: true
    },
    author_name: {
        type: String,
        required: true
    },
    Isbn_no: {
        type: String,
        unique : true,
        required: true
    },
    book_img: {
        type: Array,
        required: true
    },
    publisher: {
        type: String,
        required: true
    },
    condition: {
        type: String,
        required: true
    },
    print_type: {
        type: String,
        required: true
    },
    mrp: {
        type: Number,
        required: true
    },
    selling_price: {
        type: Number,
        required: true
    },
    saved_price: {
        type: Number,
        required: true
    },
    sale_price :{
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    publication_year: {
        type: Date,
        required: true
    },
    quantity: {
        type: Number,
        default: 1
    },
    no_Of_pages: {
        type: Number,
        required: true
    },
    language: {
        type: String,
        required: true
    },
    dimensions: {
        type: String,
        required: true
    },
    weight: {
        type: Number,
        required: true
    },
    categories: {
        type: String,
       
        required: true
    }
});

module.exports = mongoose.model('Book', bookSchema);