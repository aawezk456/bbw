const Book = require('../models/book');
const mongoose = require('mongoose');


exports.sortBy = (req, res, next) => {
    if (req.query.sortBy == 'asc') {
        const mysort = { selling_price: 1 };
        Book.find().sort(mysort).exec()
            .then(result => {
                res.status(200).json(result);
            })
            .catch(error => {
                res.status(500).json({
                    error: error
                });
            });
    }
    if (req.query.sortBy == 'desc') {
        const mysort = { selling_price: -1 };
        Book.find().sort(mysort).exec()
            .then(result => {
                res.status(200).json(result);
            })
            .catch(error => {
                res.status(500).json({
                    error: error
                });
            });
    }
}


exports.price_sort = (req,res,next)=>{
    const first = req.params.first;
    const second = req.params.second;
    Book.find({selling_price : {$gte:(first),$lte:(second)}}).exec()
    .then(result => {
        res.status(200).json(result);
    })
    .catch(error => {
        res.status(500).json({
            error: error
        });
    });
}