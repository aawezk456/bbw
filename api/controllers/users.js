const User = require('../models/user');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const config = require('../../config');
const client = require('twilio')(config.ACCOUNT_SID, config.AUTH_TOKEN);
const jwt = require('jsonwebtoken');



exports.user_signup = (req, res, next) => {
    User.find({ phonenumber: req.body.phonenumber }).exec()
        .then(user => {
            if (user.length >= 1) {
                res.status(409).json(
                    "Phonenumber already Exist"
                );
            } else {
               
                        client
                            .verify
                            .services(config.SERVICE_ID)
                            .verifications
                            .create({
                                to: `+91${req.body.phonenumber}`,
                                channel: "sms"
                            }).then(data =>{
                               res.status(200).json(
                               "Verification Sent Successfully"
                                
                               );
                            }).catch(err =>{
                                res.status(400).json("Check Phonenumber");
                            });

                       
                    }

                
           
        });
}


exports.user_login = (req, res, next) => {
    User.find({ phonenumber: req.body.phonenumber })
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({
                    message: "Auth failed"
                });
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        message: "Auth failed"
                    });
                }
                if (result) {
                    const token = jwt.sign({
                        name : user[0].name,
                        email: user[0].email,
                        phonenumber: user[0].phonenumber,
                        userId: user[0]._id
                    },
                        process.env.JWT_KEY,
                        {
                            expiresIn: "1h"
                        });
                    return res.status(200).json({
                        message: "Auth Successfull",
                        token: token
                    });
                }
                return res.status(401).json({
                    message: "Auth failed"
                });
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
}


exports.getall_users = (req, res, next) => {
    User.find()
        .select("name phonenumber email otpVerified")
        .exec()
        .then(docs => {
            if (docs.length >= 0) {
                res.status(200).json(docs);
            } else {
                res.status(404).json({
                    message: "No Entry Found"
                });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}


exports.verification = (req, res, next) => {

    if (req.query.phonenumber && (req.query.code).length === 6) {
        client
            .verify
            .services(config.SERVICE_ID)
            .verificationChecks
            .create({
                to: `+91${req.query.phonenumber}`,
                code: req.query.code
            }).then(data => {
                if (data.status === "approved") {
                    bcrypt.hash(req.body.password, 10, (err, hash) => {
                        if (err) {
                            return res.status(500).json({
                                error: err
                            })
                        } else {
                    const user = new User({
                        _id: mongoose.Types.ObjectId(),
                        name : req.body.name,
                        phonenumber: req.body.phonenumber,
                        email: req.body.email,
                        password: hash
                    });
                    user.save()
                        .then(result => {
                            console.log(result);
                            return res.status(201).json({
                                message: 'Register Successfull'
                            });
                        })
                        .catch(err => {
                            console.log(err);
                            res.status(500).json({
                                error: err
                            })
                        });
                    }
                    });
                }else{
                    res.status(400).json(
                        "Verification Failed"
                    );
                }
            }).catch(err=>{
                res.status(401).json(
                     "Otp Expired"
                );
            });
       
    } else {
        res.status(400).send({
            message: "Wrong phone number or code :(",
            phonenumber: req.query.phonenumber

        })
    }
}


exports.updateUser = (req, res, next) => {
    const id = req.params.UserId;
    User.update({ _id: id },
        {   
            name : req.body.name,
            phonenumber: req.body.phonenumber,
            email: req.body.email,
        }, (err, docs) => {
            if (err) {
                res.status(500).json({
                    error: err
                });
            } else {
                res.status(200).json({
                    message: "Updated Success",
                    docs
                });
            }

        })
}


exports.user_delete = (req, res, next) => {
    const id = req.params.UserId;
    User.deleteOne({ _id: id }).exec()
        .then(result => {
            res.status(200).json({
                message: "User deleted Successfully",
                result
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
}

exports.sendOtp = (req,res,next) =>{
    User.find({phonenumber : req.params.Phone})
    .exec()
    .then(user => {
        console.log(user)
        if(user.length > 0){
            client
            .verify
            .services(config.SERVICE_ID)
            .verifications
            .create({
                to : `+91${req.params.Phone}`,
                channel : "sms"
            })
            .then(result =>{
                res.status(200).json(result)
            })
            .catch(err =>{
                res.status(500).json({
                    error :  err
                });
            });
            
        }else{
            res.status(404).json(

                 "phonenumber doesnt exist"
            );
        }
    })
    .catch(err =>{
        res.status(500).json({
            error : err
        });
    });
}

exports.resendOtp = (req,res,next) =>{
  
            client
            .verify
            .services(config.SERVICE_ID)
            .verifications
            .create({
                to : `+91${req.params.Phone}`,
                channel : "sms"
            })
            .then(result =>{
                res.status(200).json("OTP SEND SUCCESSFULLY")
            })
            .catch(err =>{
                res.status(500).json({
                    error :  err
                });
            });
            
}

exports.forgetpw = (req, res, next) => {

    if (req.query.phonenumber && (req.query.code).length === 6) {
        client
            .verify
            .services(config.SERVICE_ID)
            .verificationChecks
            .create({
                to: `+91${req.query.phonenumber}`,
                code: req.query.code
            })  .then(data => {
                if (data.status === "approved") {
                    bcrypt.hash(req.query.password, 10, (err, hash) => {
                            if(err){
                                return res.status(500).json({
                                    error: err
                                })
                            }
                            else{
                                const myquery = { phonenumber: req.query.phonenumber };
                                const newvalue = { $set: { password : hash} };
                                User.updateOne(myquery, newvalue)
                                    .then(data => {
                                        const user = data;
                                        res.status(200).json({
                                            message: "Password Change Successfully"
                                            
                                        })
                                    })
                                    .catch(err => {
                                        res.status(500).json({
                                            error: err
                                        })
                                    });
                            }
                        });
                    res.status(200).json(
                         "User is Verified!!"
                        
                    )
                }else{
                    res.status(401).json(
                        "Verification Failed"
                        
                    );
                }
            }).catch(err=>{
                res.status(401).json(
                     "Verification Failed"
                    
                );
            });
              
       
    } else {
        res.status(400).send(
           "Wrong phone number or code :("
            

        );
    }
}
